if exists('g:loaded_octavevim')
	finish
endif
let g:loaded_octavevim = 1

let g:filetype_m = 'octave'
